<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Exception\Wrapper\Api;

use Exception;
use Throwable;

/**
 * Trait ExceptionWrapperTrait.
 *
 * Use this trait to help yourself to implement ExceptionWrapperInterface.
 *
 * @see ExceptionWrapperInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Exception\Wrapper
 */
trait ExceptionWrapperTrait
{
    /**
     * {@inheritdoc}
     */
    public function wrapException(
        string $exceptionClass,
        Throwable ...$exceptions
    ): Throwable {
        $messages = [];
        $codes = [];
        $original = null; //to preserve the exception trace from original one
        while ($previous = \array_pop($exceptions)) {
            $original = $original ?? $previous;
            $messages[] = \sprintf(
                "\n(%s): %s\n%sat %s:%s\n",
                \get_class($previous),
                $previous->getMessage(),
                $previous->getCode() ? ('code: ' . $previous->getCode() . PHP_EOL) : '',
                $previous->getFile(),
                $previous->getLine()
            );

            $codes[] = $previous->getCode();
        }

        try {
            $exception = new $exceptionClass(
                \sprintf(
                    '%s',
                    \implode(PHP_EOL, $messages)
                ),
                (int) \reset($codes),
                $original //pass original exception to merge trace with the new one
            );
        } catch (\Throwable $e) {
            // Exception class may have a different constructor
            $exception = new $exceptionClass();
        }

        return $exception;
    }

    /**
     * {@inheritdoc}
     */
    public function wrapAndThrowException(
        string $exceptionClass,
        Throwable ...$exceptions
    ): void {
        throw $this->wrapException($exceptionClass, ...$exceptions);
    }
}
