<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\Exception\Wrapper\Api;

use Throwable;

/**
 * Interface ExceptionWrapperInterface.
 *
 * Manage exception wrapping.
 *
 * @see ExceptionWrapperTrait
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\Exception\Wrapper
 */
interface ExceptionWrapperInterface
{
    /**
     * Create an instance of the supplied exception class, chaining rest of exceptions as previous.
     * When first exception supplied is an instance of expected exception class, it will be used for wrapping
     * the rest of exceptions.
     * When no exceptions are supplied, a new empty exception of the expected exception class will be returned.
     *
     * @param string    $exceptionClass
     * @param Throwable ...$exceptions
     *
     * @return Throwable
     */
    public function wrapException(
        string $exceptionClass,
        Throwable ...$exceptions
    ): Throwable;

    /**
     * Wrap exception and throw it immediately.
     *
     * @param string    $exceptionClass
     * @param Throwable ...$exceptions
     *
     * @return void
     *
     * @throws Throwable
     *
     * @see wrapException
     */
    public function wrapAndThrowException(
        string $exceptionClass,
        Throwable ...$exceptions
    ): void;
}
